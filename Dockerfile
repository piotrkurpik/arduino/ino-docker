FROM alpine/git as helper
WORKDIR /
RUN git clone https://github.com/bblanchon/ArduinoJson && \
	git clone https://github.com/sirleech/Webduino

FROM python:2.7.18@sha256:cfa62318c459b1fde9e0841c619906d15ada5910d625176e24bf692cf8a2601d
MAINTAINER kurpik.piotr@gmail.com

# https://github.com/amperka/ino/issues/55
# https://forum.arduino.cc/index.php?topic=168854.0
RUN apt-get update && apt-get install -y arduino picocom && \
	pip install ino && \
	sed -e 's#socket.h#utility/socket.h#' -i /usr/share/arduino/libraries/WiFi/WiFiClient.cpp && \ 
	sed -e 's#socket.h#utility/socket.h#' -i /usr/share/arduino/libraries/Ethernet/EthernetClient.cpp && \
	rm -rf /usr/share/arduino/libraries/Robot_Control 

COPY --from=helper /ArduinoJson/src /usr/share/arduino/libraries/ArduinoJson
COPY --from=helper /Webduino /usr/share/arduino/libraries/Webduino
COPY ino.ini /etc/ino.ini

WORKDIR /app

# Define entry point and default command.
ENTRYPOINT ["ino"]
CMD ["--help"]

