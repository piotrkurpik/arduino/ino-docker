# ino-docker #

This project contains Dockerfile to build image which can be used to compile Arduino project, upload it to board and connect to Arduino via serial port. Separate images are created for all supported boards

### content ###

This image contains:
- [Arduino IDE with libraries](https://www.arduino.cc/en/main/software). There are also workarounds for few issues applied
- [picocom](https://linux.die.net/man/8/picocom) for serial communication
- [ino](https://github.com/amperka/ino) - command line tool for interacting with Arduino
- [ArduinoJson module](https://github.com/bblanchon/ArduinoJson)
- [WebServer module](https://github.com/sirleech/Webduino)

### supported boards ###
- Arduino Uno - registry.gitlab.com/piotrkurpik/arduino/ino-docker:ino
- Arduino Mega 2560 - registry.gitlab.com/piotrkurpik/arduino/ino-docker:mega2560

### using image from container registry ###

to init Arduino project (from empty directory):

`docker run --privileged -v /dev/ttyACM0:/dev/ttyACM0 -v ${PWD}:/app registry.gitlab.com/piotrkurpik/arduino/ino-docker:ino init`

to compile Arduino project (from project directory):

`docker run --privileged -v /dev/ttyACM0:/dev/ttyACM0 -v ${PWD}:/app registry.gitlab.com/piotrkurpik/arduino/ino-docker:ino build`

to upload Arduino app (from project directory):

`docker run --privileged -v /dev/ttyACM0:/dev/ttyACM0 -v ${PWD}:/app registry.gitlab.com/piotrkurpik/arduino/ino-docker:ino upload`

to connect to Arduino via serial port (from project directory):

`docker run -it --privileged -v /dev/ttyACM0:/dev/ttyACM0 -v ${PWD}:/app registry.gitlab.com/piotrkurpik/arduino/ino-docker:ino serial`

### building and running locally ###

to build (from directory with Dockerfile):

`docker build -t ino:latest .` 

or simply

`make`

to init Arduino project (from empty directory):

`docker run --privileged -v /dev/ttyACM0:/dev/ttyACM0 -v ${PWD}:/app ino:latest init`

to compile Arduino project (from project directory):

`docker run --privileged -v /dev/ttyACM0:/dev/ttyACM0 -v ${PWD}:/app ino:latest build`

to upload Arduino app (from project directory):

`docker run --privileged -v /dev/ttyACM0:/dev/ttyACM0 -v ${PWD}:/app ino:latest upload`

to connect to Arduino via serial port (from project directory):

`docker run -it --privileged -v /dev/ttyACM0:/dev/ttyACM0 -v ${PWD}:/app ino:latest serial`

### limitations ###

at the moment Docker image:
- supports only Arduino Mega2560
