#!/bin/bash

mkdir -p /app/src
mkdir -p /app/lib

cat <<EOF > /app/src/sketch.ino 
#include <ArduinoJson.h>
#include <WebServer.h>
#include <Ethernet.h>
#include <SPI.h>

void setup() {}
void loop() {}
EOF

cd /app/
ino build

